const dataSet = require("../js_drill_5.js");

function problem2() {
  try {
    const projectStatus = dataSet.reduce(function (acc, curr) {
      let projects = curr.projects;
      projects.reduce(function (acc1, curr1) {
        if (acc[curr1.status]) {
          acc[curr1.status].push(curr1.name);
        } else {
          acc[curr1.status] = [curr1.name];
        }
      }, {});
      return acc;
    }, {});
    return projectStatus;
  } catch (error) {
    console.error("An error occurred:", error.message);
  }
}
module.exports = problem2;