const dataSet = require("../js_drill_5.js");

function problem1() {
  try {
    const employee = dataSet.reduce(function (acc, curr) {
      let obj = {};
      let name = curr.name.split(" ");
      obj.firstName = name[0];
      obj.lastName = name[1];
      let email = curr.email.split("@");
      obj.email = email[1];
      acc.push(obj);
      return acc;
    }, []);
    return employee;
  } catch (error) {
    console.error("An error occurred:", error.message);
  }
}
module.exports = problem1;