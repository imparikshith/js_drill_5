const dataSet = require("../js_drill_5.js");

function problem3() {
  try {
    const languages = dataSet.reduce(function (acc, curr) {
      curr.languages.reduce(function (acc1, curr1) {
        if (acc.includes(curr1)) {
          return;
        } else {
          acc.push(curr1);
        }
      }, {});
      return acc;
    }, []);
    return languages;
  } catch (error) {
    console.error("An error occurred:", error.message);
  }
}
module.exports = problem3;